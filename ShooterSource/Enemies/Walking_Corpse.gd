extends KinematicBody2D

const SPEED = 40
var health = 100

onready var anim = get_node("Animator")
onready var Player = get_parent().get_node("Player")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if(health < 1):
		print("Yolo'd")
		die()

		
	if not is_at_Player():
		move_and_slide(walk_to_Player())

func walk_to_Player():
	#face the player and play the walking animation
	self.look_at(Player.position)
	if not anim.is_playing():
		anim.play("walking")
	
	#move toward the player
	var direction = (Player.position - position).normalized()
	var movement = direction * SPEED 
	return movement
	
func is_at_Player():
	if(self.position.distance_to(Player.position)<25):
		return true
	else:
		return false

func die():
	get_parent().add_child(load("res://Enemies/Walking_Corpse.tscn").instance())
	queue_free()

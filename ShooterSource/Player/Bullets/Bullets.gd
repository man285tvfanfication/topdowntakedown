extends RigidBody2D

var shooter
var Enemy = preload("res://Enemies/Walking_Corpse.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	($Timer as Timer).start()


func die():
	self.queue_free()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#make it vanish after a bit so it wont cause lag
func _on_Timer_timeout():
	die()


func _on_Bullet_body_entered(body):
	print("hit")
	if(body is Enemy):
		body.health = body.health-1;


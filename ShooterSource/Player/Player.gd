extends KinematicBody2D

var Bullet = preload("res://Player/Bullets/Bullets.tscn")
var speed = 5
var moving_right 	= false
var moving_left  	= false
var moving_down  	= false
var moving_up 		= false
var flashlight_is_on = true

onready var weapon_animations = get_node("Gun/weapon_animations")
onready var	Gun = get_node("Gun")	
onready var	screen_size = get_viewport_rect().size

#non-global things that happen once when the node enters the scene
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	
	#set frame movement variables
	var movement = Vector2(0,0)
	moving_right = 	Input.is_action_pressed("move_right") 
	moving_left =	Input.is_action_pressed("move_left")
	moving_down =	Input.is_action_pressed("move_down") 
	moving_up 	=	Input.is_action_pressed("move_up")
	
	#have player face mouse
	self.look_at(get_global_mouse_position())
	
	#handle the player movement
	movement = handle_movement()
	
	#handle gun useage (movement if for potential recoil)
	if(Input.is_action_pressed("fire") and not Gun.firing_delay):
		Gun.firing_delay = true
		($Gun/rate_of_fire as Timer).start()
		movement += fire_gun()
		
	move_and_collide(movement)
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
	#handle flashlight useage
	if(Input.is_action_pressed("toggle_flashlight")):
		toggle_flashlight()

#handle player-controlled movements
func handle_movement():
	var movement = Vector2(0,0)
	if(moving_left and not moving_right):
		movement.x -= 10
	if(moving_right and not moving_left):
		movement.x += 10
	if(moving_up and not moving_down):
		movement.y -= 10
	if(moving_down and not moving_up):
		movement.y += 10
	movement = movement.normalized() * speed
	return movement

func toggle_flashlight():
	if(!weapon_animations.is_playing()):
		if(flashlight_is_on == false):
			weapon_animations.play("flashlight_on")
			flashlight_is_on = true
		else:
			weapon_animations.play("flashlight_off")
			flashlight_is_on = false
		
func fire_gun():

	#create a bullet and add it to the scene
	var bullet = Bullet.instance()
	var shoot_direction = Vector2(get_global_mouse_position() - position).normalized()
	add_collision_exception_with(bullet)
	get_parent().add_child(bullet)
	bullet.set_global_position($Gun/bullet_emitter.get_global_position())
	bullet.linear_velocity = 400 * shoot_direction
	bullet.rotation = get_global_mouse_position().angle_to_point(position)
	
	#work that movie magic
	var recoil = Vector2(0,0)
	weapon_animations.play("muzzle_flash")
	return recoil
